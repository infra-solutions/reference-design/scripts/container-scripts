#!/usr/bin/env bash

# Copyright (c) 2024, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

set -e


function usage(){
    cat <<EOF
PATH : Absolute path of tools folder
VERSION : Specify clang version to install

Examples:
${BASH_SOURCE[0]} /opt 15.0.6
${BASH_SOURCE[0]} ~/workspace/tools 18.1.7

EOF
}

if [[ ! ${#} -eq 2 ]]; then
    usage
    exit 1
fi

TOOLS_PATH="${1}"
version="${2}"

hostarch=$(uname -m)

if [ "${hostarch}" == "x86_64" ]; then
    toolchain="clang+llvm-${version}-${hostarch}-linux-gnu-ubuntu-18.04"
elif [ "${hostarch}" == "aarch64" ]; then
    toolchain="clang+llvm-${version}-${hostarch}-linux-gnu"
else
    echo "Unsupported architecture= ${hostarch} exiting now."
    exit 1
fi

archive_url="https://github.com/llvm/llvm-project/releases/download/llvmorg-${version}/${toolchain}.tar.xz"
tmp_path="/tmp/clang"
install_path=${TOOLS_PATH}/clang/${toolchain}

# Clean if any residual working folder exists
[[ -d ${tmp_path} ]] && rm -rf ${tmp_path}
mkdir -p ${tmp_path}

# Create target folder
mkdir -p ${install_path}

# Download
echo "Downloading ${toolchain}"
pushd ${tmp_path} > /dev/null
wget -nv ${archive_url}
popd > /dev/null

# Extract
echo "Extracting ${toolchain} to ${install_path}"
tar -xf ${tmp_path}/${toolchain}.tar.xz -C ${install_path} --strip-components=1

# Clean up
rm -rf ${tmp_path}
