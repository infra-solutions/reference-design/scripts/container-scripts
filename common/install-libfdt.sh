#!/usr/bin/env bash

# Copyright (c) 2023-2024, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

set -e

function usage(){
    cat <<EOF
Installs libfdt a utility library for reading and manipulating the binary format.

PATH : Absolute path of tools folder
TOOLCHAIN_VERSION : Cross compiler version of the toolchain
TOOLCHAIN_VARIANT : Cross compiler variant of the toolchain

Examples:
${BASH_SOURCE[0]} /opt 13.2.rel1 aarch64-none-linux-gnu

EOF
}

if [[ ! ${#} -eq 3 ]]; then
    usage
    exit 1
fi

TOOLS_PATH="${1}"
toolchain_version="${2}"
toolchain_variant="${3}"

hostarch=$(uname -m)
tmp_path="/tmp/dtc"

if [ "${hostarch}" == "x86_64" ]; then
    toolchain="arm-gnu-toolchain-${toolchain_version}-${hostarch}-${toolchain_variant}"
    export CC="${TOOLS_PATH}/gcc/${toolchain}/bin/${toolchain_variant}-gcc"
    SYSROOT=$(realpath $(${CC} -print-sysroot))
elif [ "${hostarch}" == "aarch64" ]; then
    # Install on host compiler SYSROOT
    SYSROOT=$(realpath $(gcc -print-sysroot))
else
    echo "Unsupported architecture= ${hostarch} exiting now."
    exit 1
fi

# Clean if any residual working folder exists
[[ -d ${tmp_path} ]] && rm -rf ${tmp_path}
mkdir -p ${tmp_path}

# Download
git clone git://git.kernel.org/pub/scm/utils/dtc/dtc.git ${tmp_path}

# Build
pushd ${tmp_path} > /dev/null
make clean
make libfdt
make DESTDIR=$SYSROOT PREFIX=/usr LIBDIR=/usr/lib64 install-lib install-includes
popd > /dev/null

# Clean up
rm -rf ${tmp_path}
