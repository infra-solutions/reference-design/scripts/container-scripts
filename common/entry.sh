#!/bin/bash

function log()
{
    if [[ -v VERBOSITY && $VERBOSITY -ne 0 ]]; then
        echo "log:entry.sh "$*
    fi
}

function is_ci_environment()
{
    # Check for CI environment variables to skip ARCADE setup
    if [[ -v GITLAB_CI ]] && $GITLAB_CI; then echo true && return; fi
    if [[ -v KUBERNETES_SERVICE_HOST ]]; then echo true && return; fi
    echo false
}

if $(is_ci_environment); then
    export VERBOSITY=1
    log "CI environment detected. Will execute immediately" $*
    exec "$@"
fi

if [[ -v ARCADE_USER && -v ARCADE_UID && -v ARCADE_GID ]]; then
    log "Adding $ARCADE_USER group with $ARCADE_GID group id."
    groupadd -g $ARCADE_GID $ARCADE_USER

    log "Adding $ARCADE_USER user with $ARCADE_UID user id."
    useradd -NM -d /home/$ARCADE_USER -s /bin/bash -u $ARCADE_UID -g $ARCADE_GID -G sudo $ARCADE_USER

    log "Adding $ARCADE_USER to sudoers."
    echo "$ARCADE_USER ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/$ARCADE_USER

    # Set HOME to user home folder and move skeleton files unless HOME folder explicitly mounted
    export HOME=/home/$ARCADE_USER
    if [[ ! -f ${HOME}/.bashrc ]]; then
        chown $ARCADE_USER:$ARCADE_USER $HOME
        for file in $( find /etc/skel/ -maxdepth 1 -mindepth 1 -name ".*" ); do
            install -o ${ARCADE_USER} -g ${ARCADE_USER} -m 600 ${file} ${HOME}
        done
    fi

    # Remove entrypoint-only environment variables
    unset ARCADE_GID ARCADE_UID

    # Change to HOME folder if we haven't provided any WORKDIR
    if [[ "${#PWD}" -eq 1 ]]; then
        cd;
    fi
    if [[ $# -ne 0 ]]; then
        log "Executing =" "$@"
    fi
    exec setpriv --reuid=$ARCADE_USER --regid=$ARCADE_USER --init-groups "$@"
else
    export VERBOSITY=1
    log "At least one ARCADE user detail is missing. Exiting now!"
    log "To run container as root user please add --entrypoint=\"/bin/bash\" to the docker run command."
fi
