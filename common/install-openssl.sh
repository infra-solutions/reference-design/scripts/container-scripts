#!/usr/bin/env bash

# Copyright (c) 2023-2024, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

set -e

function usage(){
    cat <<EOF
This script uses positional arguments. There is one mandatory and two optional
arguments are available.

PATH : Absolute path of tools folder
VERSION : Specify OpenSSL version to install. If it's not provided it will use default version
CHECKSUM: It's mandatory if VERSION is provided. SHA256 checksum of the archive file.

Examples:
${BASH_SOURCE[0]} /opt
${BASH_SOURCE[0]} /opt 3.0.8 6c13d2bf38fdf31eac3ce2a347073673f5d63263398f1f69d0df4a41253e4b3e

EOF
}

if [[ ! ${#} -eq 1 ]] && [[ ! ${#} -eq 3 ]]; then
    usage
    exit 1
fi

TOOLS_PATH=${1}
version="${2:-3.0.8}"
checksum="${3:-6c13d2bf38fdf31eac3ce2a347073673f5d63263398f1f69d0df4a41253e4b3e}"
name="openssl-${version}"

# Clean if any residual working folder exists
[[ -d /tmp/${name} ]] && rm -rf /tmp/${name}

# Create temp folder to download and compile
mkdir -p /tmp/${name}
pushd /tmp/${name} > /dev/null

# Download
echo "Downloading ${name}"
wget -nv https://www.openssl.org/source/${name}.tar.gz

# Verify integrity
echo "${checksum} ${name}.tar.gz" | sha256sum -c

# Extract
tar -xzf /tmp/${name}/${name}.tar.gz --strip-components=1

# Configure OpenSSL. According to documentation advised to set prefix and openssldir to same location
# https://wiki.openssl.org/index.php/Compilation_and_Installation#PREFIX_and_OPENSSLDIR
echo "Installing ${name}"
./Configure --libdir=lib --prefix=${TOOLS_PATH}/openssl --openssldir=${TOOLS_PATH}/openssl --api=1.0.1

# Build
make -j

# Install without documentation. For documentation installation please use install target.
make install_sw
popd > /dev/null

# Clean up
rm -rf /tmp/${name}
